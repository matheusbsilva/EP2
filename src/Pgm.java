import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBufferByte;
import java.awt.image.Kernel;


public class Pgm extends Image {
	private int byteFile = 0;
	private char character = 0x00;
	private String message= "";
	
	
	Pgm(String path){
		this.path = path;
	}
	
	
	public String decode (){
		int position = 5000; //Posição Inicial da Mensagem
		int count = 0;
		while(true){
			character = 0x00;
			for(count = 0 ;count < 8;count++,position++){
				byteFile = pixels[position];
				character = (char) ((character << 1)| byteFile & 0x01);
			}
			
			if(character == '#')
				break;
			message += character;
		}
		return message;
	}
	
	public BufferedImage negativeFilter(){
		byte bt;
		int count = 0;
		
		BufferedImage imageOutNegative = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
		byte [] pixelsNegative = ((DataBufferByte) imageOutNegative.getRaster().getDataBuffer()).getData(); 
		while(count < (width*height)){
			bt = pixels[count];
			pixelsNegative[count] = (byte)(maxColor - bt);
			count++;
		}
		return imageOutNegative;
	}
	
	public BufferedImage blurFilter(){
		
		BufferedImage imageOutBlur = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
		
		
		float[] matrix = new float[9];
		for (int i = 0; i < 9; i++)
			matrix[i] = 1.0f/9.0f;

	    	BufferedImageOp op = new ConvolveOp( new Kernel(3, 3, matrix), ConvolveOp.EDGE_ZERO_FILL, null );
	    	Object blurredImage = op.filter(imageIn, imageOutBlur);
		
		return imageOutBlur;
	}
	
	public BufferedImage sharpenFilter(){
		BufferedImage imageOutSharpen = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
		
		float[] matrix = {0,-1,0,-1,5,-1,0,-1,0};
		

    	BufferedImageOp op = new ConvolveOp( new Kernel(3, 3, matrix), ConvolveOp.EDGE_ZERO_FILL, null );
    	Object blurredImage = op.filter(imageIn, imageOutSharpen);
    	
    	return imageOutSharpen;
	}
}
