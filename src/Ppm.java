import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;


public class Ppm extends Image {
	
	
	Ppm(String path){
		this.path = path;
	}
	
	public BufferedImage redFilter(){
		BufferedImage redImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int count = 0;
		byte[] redBytes= new byte[inversionOfBytes.length];
		
		while(count < inversionOfBytes.length){
			redBytes[count] = inversionOfBytes[count];
			 count++;
		}
		count = 0;
		while(count< redBytes.length){
			if((1+count) % 3 != 0)
				redBytes[count] = 0;
			count++;
		}
		
        pixels = ((DataBufferByte) redImage.getRaster().getDataBuffer()).getData();
        
        count=0;
		while(count<3*width*height){
			pixels[count] = (byte) redBytes[count];
			count++;
		}
		return redImage;
	}
	
	public BufferedImage greenFilter(){
		BufferedImage greenImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int count = 0;
		byte[] greenBytes= new byte[inversionOfBytes.length];
		
		while(count < inversionOfBytes.length){
			greenBytes[count] = inversionOfBytes[count];
			count++;
		}
		count = 0;
		while(count< inversionOfBytes.length){
			if((2+count) % 3 != 0)
				greenBytes[count] = 0;
			count++;
		}
		
        pixels = ((DataBufferByte) greenImage.getRaster().getDataBuffer()).getData();
        
        count=0;
		while(count<3*width*height){
			pixels[count] = (byte) greenBytes[count];
			count++;
		}
		return greenImage;
	}
	
	public BufferedImage blueFilter(){
		BufferedImage blueImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int count = 0;
		byte[] blueBytes= new byte[inversionOfBytes.length];
		
		while(count < inversionOfBytes.length){
			blueBytes[count] = inversionOfBytes[count];
			count++;
		}
		count = 0;
		while(count< blueBytes.length){
			if((count) % 3 != 0)
				blueBytes[count] = 0;
			count++;
		}
		
        pixels = ((DataBufferByte) blueImage.getRaster().getDataBuffer()).getData();
        
        count=0;
		while(count<3*width*height){
			pixels[count] = (byte) blueBytes[count];
			count++;
		}
		return blueImage;
	}
	
	public BufferedImage negativeFilter(){
		BufferedImage negativeImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int count = 0;
		byte[] negativeBytes= new byte[inversionOfBytes.length];
		
		while(count < inversionOfBytes.length){
			negativeBytes[count] = inversionOfBytes[count];
			count++;
		}
		count = 0;
		while(count< negativeBytes.length){
			negativeBytes[count] = (byte) (maxColor-inversionOfBytes[count]);
			count++;
		}
		
        pixels = ((DataBufferByte) negativeImage.getRaster().getDataBuffer()).getData();
        
        count=0;
		while(count<3*width*height){
			pixels[count] = (byte) negativeBytes[count];
			count++;
		}
		return negativeImage;
		
	}

}
