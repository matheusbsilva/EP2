
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.io.File;
import javax.swing.ImageIcon;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JTextField;


public class MainFrame extends JFrame {

	private JFrame frame;	

	public MainFrame() {
		Interface();
	}

	
	private void Interface(){
		frame = new JFrame();
		JFileChooser chooser = new JFileChooser();
		File workingDirectory = new File(System.getProperty("user.dir"));
		chooser.setCurrentDirectory(workingDirectory);
		chooser.showOpenDialog(getParent());
		String filename = chooser.getSelectedFile().getName();
		String extension = filename.substring(filename.lastIndexOf("."),filename.length());

		if(".pgm".equals(extension)){
			Pgm imagePgm = new Pgm(chooser.getSelectedFile().getAbsolutePath());
			
			
			frame.getContentPane().setLayout(new FlowLayout());
			frame.getContentPane().setBackground(Color.BLACK);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePgm.readImage()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePgm.blurFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePgm.sharpenFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePgm.negativeFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
		
			JTextField message = new JTextField (imagePgm.decode());
			message.setHorizontalAlignment(JTextField.LEFT);

			
			
			frame.add(message);
			

			frame.setSize(1280,768);
			frame.setVisible(true);
			
		}
		if(".ppm".equals(extension)){
			Ppm imagePpm = new Ppm(chooser.getSelectedFile().getAbsolutePath());
			
			
			frame.getContentPane().setLayout(new FlowLayout());
			frame.getContentPane().setBackground(Color.BLACK);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePpm.readImage()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePpm.redFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePpm.greenFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePpm.blueFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			frame.getContentPane().add(new JLabel(new ImageIcon((imagePpm.negativeFilter()).getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
			
			
			

			frame.setSize(1280,768);
			frame.setVisible(true);
			
		}
		
			
		

		
	}

}
