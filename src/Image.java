import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

abstract class Image {
	
	protected String path;
	protected String magicNumber;
	protected int width;
	protected int height;
	protected int maxColor;
	protected byte[] pixels = null;
	protected BufferedImage imageIn = null;
	protected byte[] loadPixels;
	protected byte[] inversionOfBytes;
	
	public BufferedImage readImage(){
		try {
			FileInputStream file = new FileInputStream(path);
			int count = 0;
			byte bt;
			
			String line;
			
			line = Ppm.readLine(file);
			magicNumber = line;
			if("P6".equals(line)||"P5".equals(line)){
				line = Image.readLine(file);
				
				while(line.startsWith("#")){
					line = Image.readLine(file);
				}
				
				
				Scanner sizes = new Scanner (line);
				if(sizes.hasNext() && sizes.hasNextInt()){
					width = sizes.nextInt();
				}
				
				if(sizes.hasNext() && sizes.hasNextInt()){
					height = sizes.nextInt();
				}
				sizes.close();
				
				line = Image.readLine(file);
				sizes = new Scanner(line);
				maxColor = sizes.nextInt();
			
				sizes.close();
			
				if("P6".equals(magicNumber)){

					imageIn = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
	
					int i,j,a;
					j=2;
					a=j;
					
					loadPixels=new byte[height*width*3];
					for(i=0;i<loadPixels.length;i++){
						loadPixels[i]=(byte)file.read();
						
					}
					inversionOfBytes = new byte[loadPixels.length];
			        byte[] pixels = new byte[loadPixels.length];
			        for (i = 0; i < loadPixels.length; i++) {
			            inversionOfBytes[j] = (byte) loadPixels[i];
			            j--;
			            if ((i + 1) % 3 == 0) {
			                j = a + 3;
			                a += 3;
			            }
			        }
			        pixels = ((DataBufferByte) imageIn.getRaster().getDataBuffer()).getData();
			        
					while(count<3*width*height){
						pixels[count] = (byte) inversionOfBytes[count];
						count++;
					}
					
				}
				if("P5".equals(magicNumber)){
					imageIn = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
					pixels = ((DataBufferByte) imageIn.getRaster().getDataBuffer()).getData();
					
					
					while(count <(width * height)){
						bt = (byte) file.read();
						pixels[count] = bt;
						count++;
						
					}
					
				}
				
			}
			else
				System.err.println("Tipo de image inválida");
			
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
		return imageIn;
	}
	
	public String getMagicNumber(){
		return magicNumber;
	}
	public void setPath(String path){
		this.path = path;
	}
	
	public String getPath(){
		return path;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public static String readLine(FileInputStream file){
		String linha= "";
		byte bb;
		try {
			while((bb = (byte) file.read()) != '\n'){
				linha += (char) bb; 
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return linha;
	}
	
	
	
}
