Matheus Batista Silva 15/0018029
================================
I. Informações sobre o programa:
===============================

Programa criado no Eclipse Mars.2 Release (4.5.2), rodando no sistema operacional Elementary OS. Recomenda-se a utilização de um sistema com arquitetura 64 bits.

II.Utilização do programa:
==========================

O programa deverá ser executado no Eclipe, ou em uma IDE que suporta java da sua preferência. Após executar o programa, será pedido que selecione um arquivo para prosseguir com a execução, o arquivo deverá ser uma image do formato PGM ou PPM(raw), qualquer outro tipo de arquivo é considerado inválido para a execução do programa.

PGM
---

Após a seleção do arquivo aparecerá uma janela que no caso do tipo de arquivo PGM mostrará, primeiramente a imagem original lida, em seguida a imagem com o filtro Blur aplicado, depois com o filtro Sharpen, e por último o filtro Negativo, e logo abaixo uma caixa de texto com a mensagem contida dentro da imagem.

PPM
---

No caso do tipo de arquivo PPM a janela mostrará, primeiramente a imagem original, em seguida a imagem com o filtro Red, filtro Green, filtro Blue e por último o filtro Negativo

Observaçoes:
===========

O inicío da mensagem escondida na PGM está como 5000, pois é a posição na imagem1.pgm,imagem2.pgm e imagem3.pgm. Para fazer a leitura da mensagem da imagem Lena.pgm é necessário alterar no código para a posição 50008(pois a mensagem começa com "#"), a parte onde deve ser alterada está assinalada com um comentário na classe Pgm.java. Ocorre um bug com a caixa de texto ao tentar mostrar a mensagem da lena na caixa de texto do java, sendo necessário fazer uma seleção com o cursos do mouse na caixa de texto para que a mensagem apareça.
